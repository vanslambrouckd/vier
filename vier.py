#!/usr/bin/python2
# -*- coding: utf-8 -*-

import sys
import time
from robobrowser import RoboBrowser
from urlparse import urlparse
import os
from settings import USERNAME, PASSWORD, BASEDIR_DOWNLOADS
import pickle
import requests
import urllib2

class Vier:
    def __init__(self):
        self.browser = RoboBrowser(parser="html.parser")
        self.setCookies()
        self.BASE_URL = 'http://vier.be'
        self.BASE_URL_VOD = "http://vod.streamcloud.be/vier_vod/mp4:_definst_"

    def login(self):
        self.browser.open("http://www.vier.be")
        signin_form = self.browser.get_form("user-login-form")
        signin_form['name'].value = USERNAME
        signin_form['pass'].value = PASSWORD
        self.browser.submit_form(signin_form)

        self.saveCookies(self.browser.session.cookies, "vier.cookie")
        return self.browser.session.cookies

    def saveCookies(self, requests_cookiejar, filename):
        with open(filename, 'wb') as f:
            pickle.dump(requests_cookiejar, f)

    def loadCookies(self, filename):
        with open(filename, 'rb') as f:
            return pickle.load(f)

    def setCookies(self):
        if (os.path.isfile("vier.cookie")):
            self.cookies = self.loadCookies("vier.cookie")
            print("Loaded login cookie")
        else: 
            self.cookies = self.login()
            print("No login cookie found - logged in")

    def getSeries(self):
        self.browser.open("http://www.vier.be/volledige-afleveringen")
        tmp_titles = self.browser.select(".video-playlist-slideshow-block .brick-title")
        series = []
        titles = []
        for title in tmp_titles:
            titles.append(title.text)

        tmp_urls_all_episodes = self.browser.select(".video-playlist-slideshow-block .more-link a")
        urls_all_episodes = []
        for link in tmp_urls_all_episodes:
            urls_all_episodes.append(link['href'])

        series_index = 0
        for title in titles:
            url = urls_all_episodes[series_index]
            item = {'title': title, 'href': url}
            series.append(item)
            series_index += 1

        return series

    def downloadFile(self, source, destination):
        #de chunklist kan blijkbaar om een of andere reden via requests niet gedownload worden, dus via urllib2
        response = urllib2.urlopen(source)
        ret = response.read()
        with open(destination, 'wb') as f:
            f.write(ret)

        return destination

        
    def downloadVideo(self, video_parts, path_download):
        for part in video_parts:
            dest = path_download + '/' + part['file']
            source = part['base_url'] + '/' + part['file']
            print("downloading %s" % dest)
            self.download(source, dest)
            
    def getEpisodeParts(self, filename, chunklist):
        #chunklist parts in array inlezen
        lines = tuple(open(chunklist))        
        nr_chunks = self.getNrChunks(chunklist)
        print('(%s parts)' % nr_chunks)
        video_parts = []
        for index, line in enumerate(lines):
            #print(index, line)
            if (index > 4):
                if (line.startswith('media_')):
                    line = line.rstrip('\n') #remove newline
                    #http://vod.streamcloud.be/vier_vod_geo/mp4:_definst_/theskyisthelimit/s3/20160412_afl10_10_geboden_willy_naessens.mp4/media_w377560393_2.ts
                    item = {'base_url': self.BASE_URL_VOD + '/' + filename, 'file': line}
                    video_parts.append(item)
        return video_parts

    def getNrChunks(self, m3u8file):
        with open(m3u8file, 'r') as f:
            lines = f.readlines()
            line = lines[-2] #'media_w640379623_251.ts'
            pos = line.rfind('_')+1
            line = line[pos:]
            return line.rsplit('.')[0]

    def getEpisode(self, url):
        #http://vod.streamcloud.be/vier_vod_geo/mp4:_definst_/theskyisthelimit/s3/20160412_afl10_10_geboden_willy_naessens.mp4/chunklist_w1537757030.m3u8
        url_info = urlparse(url)
        arr_path = url_info.path.split('/')
        series_name = arr_path[2]
        episodes_name = arr_path[4]
        data_nid = arr_path[-1]
        dir_name = BASEDIR_DOWNLOADS + "/" + series_name + '/' + episodes_name
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        #data_nid = os.path.split(url_info.path)[1]
        cls = 'video_player_'+data_nid+'_data'
        self.browser.open(url, cookies=self.cookies)
        #print(self.browser.parsed())
        video_element = self.browser.select('.'+cls) 
        attrs = video_element[0].attrs
        filename = attrs['data-filename']+'.mp4' #theskyisthelimit/s3/20160412_afl10_10_geboden_willy_naessens


        url_playlist_local = dir_name+'/playlist.m3u8'

        if not os.path.exists(url_playlist_local):
            url_playlist = self.BASE_URL_VOD + '/' + filename + "/playlist.m3u8"
            self.downloadFile(url_playlist, url_playlist_local)

        lines = tuple(open(url_playlist_local))
        chunklist = lines[-1]
        chunklist = self.BASE_URL_VOD + '/' + filename + '/' + chunklist
        chunklist_local = dir_name+'/chunklist.m3u8'
        if not os.path.exists(chunklist_local):
            self.downloadFile(chunklist, chunklist_local)

        video_parts = self.getEpisodeParts(filename, chunklist_local)
        #kijken welke video_parts reeds gedownload werden

        self.downloadVideo(video_parts, dir_name)
       

    def GetHumanReadable(self, size,precision=2):
        suffixes=['B','KB','MB','GB','TB']
        suffixIndex = 0
        while size > 1024 and suffixIndex < 4:
            suffixIndex += 1 #increment the index of the suffix
            size = size/1024.0 #apply the division
        return "%.*f%s"%(precision,size,suffixes[suffixIndex])

    def download(self, url, destination):
        if os.path.exists(destination):
            resume_byte_pos = os.path.getsize(destination)
        else:
            resume_byte_pos = 0

        if (resume_byte_pos > 0):
            #print("resuminging download %s " % destination)
            resume_header = {'Range': 'bytes=%s-' % resume_byte_pos}
        else:
            #print("starting download %s " % destination)
            resume_header = {}
        r = self.browser.session.get(url, headers=resume_header,stream=True)

        filesize = float(r.headers['Content-Length'])
        filesize_formatted = self.GetHumanReadable(filesize)

        starttime_download = time.time()
        if r.status_code in [200, 206]:
            #print('resume from %s ' % resume_byte_pos)
            #print(r.headers)            
            CHUNK_SIZE = 8192
            bytes_read = 0
            with open(destination, 'ab') as f:
                itrcount = 1
                for chunk in r.iter_content(CHUNK_SIZE):
                    itrcount  = itrcount + 1
                    f.write(chunk)
                    bytes_read += len(chunk)
                    total_bytes_downloaded = resume_byte_pos + bytes_read
                    percentage = self.calculatePercentage(total_bytes_downloaded, filesize)
                    total_bytes_downloaded_formatted = self.GetHumanReadable(total_bytes_downloaded, 2)
                    
                    p = float(percentage/100)
                    self.drawProgressBar(p, starttime_download, 100)
        else:
            pass
            #print(r.status_code)
        r.close()

    def drawProgressBar(self, percent, start, barLen=20):
        sys.stdout.write("\r")
        progress = ""
        for i in range(barLen):
            if i < int(barLen * percent):
                progress += "="
            else:
                progress += " "

        elapsedTime = time.time() - start;
        estimatedRemaining = int(elapsedTime * (1.0/percent) - elapsedTime)

        if (percent == 1.0):
            sys.stdout.write("[ %s ] %.1f%% Elapsed: %im %02is ETA: Done!\n" % 
                (progress, percent * 100, int(elapsedTime)/60, int(elapsedTime)%60))
            sys.stdout.flush()
            return
        else:
            sys.stdout.write("[ %s ] %.1f%% Elapsed: %im %02is ETA: %im%02is " % 
                (progress, percent * 100, int(elapsedTime)/60, int(elapsedTime)%60,
                 estimatedRemaining/60, estimatedRemaining%60))
            sys.stdout.flush()
            return

    def calculatePercentage(self, start, end):
        return (start/end)*100

    def listSeries(self):
        self.clearScreen()
        series = self.getSeries()

        for index, serie in enumerate(series):
            str = "[%d] %s"
            print(str % (index, serie['title']))

        series_number = raw_input("Please choose series number: ")
        try:
            chosen_serie = series[int(series_number)]
        except:
            self.listSeries()

        self.listEpisodesList(chosen_serie)

    def clearScreen(self):
        os.system('clear')

    def run(self):
        self.listSeries()

    def formatString(self, str):
        return str.strip('\n').strip().encode('utf-8')

    def getEpisodesList(self, url):
        #http://www.vier.be/volledige-afleveringen/1595901
        self.browser.open(url)
        tmp_titles = self.browser.select(".node-video .field-name-title a")
        #print(tmp_titles)
        # series = []
        items = [];
        for title in tmp_titles:
            item = {
                'title': self.formatString(title.text),
                'href': self.formatString(title['href'])
            }
            items.append(item)

        return items

    def listEpisodesList(self, chosen_serie):
        self.clearScreen()
        series_list = self.getEpisodesList(self.BASE_URL+chosen_serie['href'])

        str_chosen_serie = "Current series: %s" % chosen_serie['title']
        print(str_chosen_serie)
        streep = ''
        for c in str_chosen_serie:
            streep += "-"

        print(streep)

        for index, listitem in enumerate(series_list):
            str = "[%d] %s"
            print(str % (index, listitem['title'])) 

        print('[b] to go back')
        print('[a] download all') 

        list_number = raw_input("Please choose item number: ")
        items_to_download = []
        if (list_number == 'b'):
            self.listSeries()
        elif (list_number == 'a'):            
            items_to_download = series_list
        else:
            try:
                items_to_download.append(series_list[int(list_number)])
            except ValueError:
                self.listEpisodesList(chosen_serie) 

        for chosen_item in items_to_download:
            print('Downloading %s' % chosen_item['title'])
            self.getEpisode(self.BASE_URL+'/'+chosen_item['href'])

        self.listEpisodesList(chosen_serie)

vier = Vier()
vier.run()
#a = vier.getNrChunks('/Users/david/episodes/vier/theskyisthelimit/aflevering-van-12-april/chunklist.m3u8')
#print(a)
#a = vier.getNrChunks('http://vod.streamcloud.be/vier_vod/mp4:_definst_/bartelinhetwild/volledigeafleveringen/20170330_bartelwild_afl1_tatyana_axel.mp4/playlist.m3u8')
#vier.getEpisode('http://vod.streamcloud.be/vier_vod/mp4:_definst_/bartelinhetwild/volledigeafleveringen/20170330_bartelwild_afl1_tatyana_axel.mp4/chunklist.m3u8')
#vier.getEpisode('http://vod.streamcloud.be/vier_vod_geo/mp4:_definst_/theskyisthelimit/s3/20160412_afl10_10_geboden_willy_naessens.mp4/chunklist_w1537757030.m3u8')
#http://vod.streamcloud.be/vier_vod_geo/mp4:_definst_/theskyisthelimit/s3/20160412_afl10_10_geboden_willy_naessens.mp4/chunklist_w1537757030.m3u8